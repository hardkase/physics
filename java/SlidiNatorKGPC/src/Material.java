
public enum Material 
{
	Null(0),
	Steel(7800),//very dense
	Balsa(120),//not dense
	Glass(2400);//medium dense - probably could have leveraged materials for fluids as well
	private float density;
	private Material(float density)
	{
		this.density=density;
	}
	public float getDensity()
	{
		return this.density;
	}
}
