
public class VectorUtility
{
	public static Vector add2V(Vector v1, Vector v2) //add two vectors
	{
		Vector v3 = new Vector(0,0,0);
		v3.setX(v1.getX()+v2.getX());
		v3.setY(v1.getY()+v2.getY());
		v3.setZ(v1.getZ()+v2.getZ());
		System.out.println(v3.toString());
		return v3;
	}
	public static Vector appVel(Vector v1, Vector v2)//compute apparent velocity
	{
		Vector v3=new Vector(0,0,0);
		v3.setX(v1.getX()-v2.getX());
		v3.setY(v1.getY()-v2.getY());
		v3.setZ(v1.getZ()-v2.getZ());
		System.out.println(v3.toString());
		return v3;
	}
	public static float getMagnitude(Vector v1)//dot product, one vector
	{
		float mag, x, y, z;
		x = v1.getX();
		y=v1.getY();
		z=v1.getZ();
		mag= (float)Math.sqrt((x*x)+(y*y)+(z+z));
		System.out.println(mag);
		return mag;
	}
	public static float getMagnitude2(Vector v1, Vector v2)//dot product of two vectors
	{
		float mag2, x1, x2, y1, y2, z1, z2;
		x1=v1.getX();
		y1=v1.getY();
		z1=v1.getZ();
		x2=v2.getX();
		y2=v2.getY();
		z2=v2.getZ();
		mag2 = (float)Math.sqrt((x1*x2)+(y1*y2)+(z1+z2));
		System.out.println(mag2);
		return mag2;
	}
	public static Vector multVecCo (float d1, Vector v1)//Multiply a vector by a coefficient
	{
		float x = v1.getX();
		float y = v1.getY();
		float z= v1.getZ();
		v1.setX(x*d1);
		v1.setY(y*d1);
		v1.setZ(z*d1);
		System.out.println(v1.toString());
		return v1;
	}
	public static Vector crossProd(Vector v1, Vector v2)
	{
		Vector rXv = new Vector(0,0,0);
		float x1,x2,y1,y2,z1,z2;
		x1=v1.getX();
		x2=v2.getX();
		y1=v1.getY();
		y2=v2.getY();
		z1=v1.getZ();
		z2=v2.getZ();
		rXv.setX((y1*z2)-(y2*z1));
		rXv.setY(-((x1*z2)-(x2*z1)));
		rXv.setZ((x1*y2)-(x2*y1));
		System.out.println(rXv.toString());
		return rXv;
		
	}
	public static Vector calcDrag(Vector v1, Vector v2, float f1, float f2, float f3)
	//1/2*(fluid dens)*(surface area orb)*(dragCo)*(magnitude velA)(velA)
	//(velA, drag, vMag, CD, drag1)
	{
		float dVco=f3*f2*f1;
		v2 = multVecCo(dVco, v1);//drag vector coefficient * velA
		System.out.println(v2.toString());
		return v2;
	}
	public static Vector calcMagnus(Vector v1, Vector v2, float d1, float d2, float d3, float d4)
	//(velA, magnus, drag1, radius, rMag, vMag)
	//magnitude of magnus in N * direction of Magnus
	//Fm=drag1*r*rMag*vMag
	//DIRm= cross prod velA*magnus/magnitude[dot prod] velA*magnus
	{
		float force = d1*d2*d3*d4;
		Vector rXv = crossProd(v1, v2);
		float rvMag=getMagnitude(rXv);
		Vector v3 = multVecCo(rvMag, rXv);
		Vector v4 = multVecCo(force, v3);
		System.out.println("Magnus: "+v4.toString());
		return v4;
	}
	public static Vector calcFnet(Vector v1, Vector v2, Vector v3)
	//(grav, drag, magnus)//add grav+drag, add 
	{
		Vector v4 = add2V(v1, v2);
		Vector v5 = add2V(v3, v4);
		System.out.println("FNet: "+v5.toString());
		return v5;
	}
	public static Vector calcAccel(Vector v1, float d1)
	{
		Vector v2 = multVecCo((float)(1.0/d1), v1);
		return v2;
	}
	public static Vector calcPosition(Vector v1, Vector v2, float d1)//posn, velN, H
	{
		Vector v3 = multVecCo(d1, v2);
		Vector v4 = add2V(v1, v3);
		return v4;
	}
	public static Vector calcVel(Vector v1, Vector v2, float d1)//velN, accN, H
	{
		Vector v3 = multVecCo(d1, v2);
		Vector v4 = add2V(v1, v3);
		return v4;
	}
	
}
